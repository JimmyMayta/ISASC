-- MariaDB dump 10.17  Distrib 10.5.6-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: ISASC_Database
-- ------------------------------------------------------
-- Server version	10.5.6-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bancos`
--

DROP TABLE IF EXISTS `bancos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bancos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `descripcion` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bancos`
--

LOCK TABLES `bancos` WRITE;
/*!40000 ALTER TABLE `bancos` DISABLE KEYS */;
INSERT INTO `bancos` VALUES (5,'BNB','Banco Nacional de Bolivia',1,'2020-01-18 01:29:45','2020-01-18 01:29:45'),(6,'Banco de Crédito de Bolivia S.A.','Banco de Crédito de Bolivia S.A.',1,'2020-01-18 01:30:07','2020-01-18 01:30:07'),(7,'BancoSol','BancoSol',1,'2020-01-18 01:30:41','2020-01-18 01:30:41'),(99,'BANCO BISA','BANCO BISA',1,NULL,NULL),(100,'Banco Union S.A. - El Banco de los Bolivianos','Banco Union S.A. - El Banco de los Bolivianos',1,'2020-02-11 02:37:43','2020-02-11 02:37:43');
/*!40000 ALTER TABLE `bancos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imagen` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categorias_nombre_unique` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (1,'Desarrollo de Sistemas','Desarrollo de Sistemas','JRKwYoaLA4DrASOq.jpg',1,'2019-08-25 07:42:47','2020-11-11 02:28:12'),(2,'Sistemas de Reservas','Sistemas de Reservas','nnBqwodPSZeh0LE8.jpg',1,'2019-08-25 07:44:01','2020-11-11 01:24:35'),(3,'Equipo de Desarrollo','Equipo de Desarrollo (Analistas de Sistemas)','RbO0EMJ3hzh5iByo.jpg',1,'2019-08-25 07:44:41','2020-11-10 23:50:33'),(4,'Motores de Sistemas','Motores de Sistemas (Nucleo)','PMicUb5Y2KnlfwkU.jpg',1,'2019-08-25 07:45:08','2020-11-10 23:48:23'),(5,'Publicidad','Publicidad','Va12a0Ca9rc4OfBS.png',1,'2019-08-25 07:45:45','2020-11-10 23:46:38'),(6,'Hosting','Hosting','MvDKUHgI5ZvMtMOz.png',1,'2019-08-25 07:46:13','2020-11-08 11:44:43'),(7,'BETM','Bolivia en tus Manos','t2MzCI0V11fSMOCq.png',1,'2019-08-25 07:47:01','2020-11-08 11:58:45'),(8,'Pagina Web','Pagina Web','KeSRS5IJ9HplqmGy.png',1,'2019-08-25 07:47:20','2020-11-20 08:21:54');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_documento` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_documento` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clientes_nombre_unique` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Marisol','CEDULA','3232837','Cdla Torresol','747374734','marisol@yahoo.com','2019-08-25 07:58:01','2020-11-21 09:42:08'),(2,'Helena','CEDULA','67364373','Plaza murrillo','2439049304','helena@gmail.com','2019-08-25 07:59:12','2020-11-12 02:58:56'),(3,'Adam Montesco','CEDULA','3874834','Ave. juan de la cruz','24040404','adam@hotmail.com','2019-08-25 08:00:16','2020-11-12 02:58:21'),(4,'Teresa','CEDULA','90438473874','ave. libertad','2834784','teresa@gmail.com','2020-11-08 13:53:50','2020-11-17 03:29:48');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes_banco`
--

DROP TABLE IF EXISTS `clientes_banco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes_banco` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idcliente` int(10) unsigned NOT NULL,
  `idbanco` int(10) unsigned NOT NULL,
  `banco` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo_cta` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cuenta` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clientes_banco_idcliente_foreign` (`idcliente`),
  KEY `clientes_banco_idbanco_foreign` (`idbanco`),
  CONSTRAINT `clientes_banco_idbanco_foreign` FOREIGN KEY (`idbanco`) REFERENCES `bancos` (`id`) ON DELETE CASCADE,
  CONSTRAINT `clientes_banco_idcliente_foreign` FOREIGN KEY (`idcliente`) REFERENCES `clientes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes_banco`
--

LOCK TABLES `clientes_banco` WRITE;
/*!40000 ALTER TABLE `clientes_banco` DISABLE KEYS */;
INSERT INTO `clientes_banco` VALUES (3,1,5,'Bolivariano','CORRIENTE','234789654','Registrada','2020-01-18 16:56:59','2020-01-18 16:56:59'),(4,2,7,'Pacifico','CORRIENTE','45578654','Registrada','2020-01-18 16:57:56','2020-01-18 16:57:56'),(5,3,7,'Pacifico','CORRIENTE','45764323','Registrada','2020-01-20 05:35:17','2020-01-20 05:35:17'),(6,2,100,'Banco General Rumiñahui','CORRIENTE','23364578','Registrada','2020-02-11 02:39:24','2020-02-11 02:39:24'),(7,3,99,'BANCO BISA','0','327942837948723','Registrada','2020-11-21 10:44:58','2020-11-21 10:44:58');
/*!40000 ALTER TABLE `clientes_banco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes_tarjetas`
--

DROP TABLE IF EXISTS `clientes_tarjetas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes_tarjetas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idcliente` int(10) unsigned NOT NULL,
  `idtarjeta` int(10) unsigned NOT NULL,
  `tarjeta` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idbanco` int(10) unsigned NOT NULL,
  `ntarjeta` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clientes_tarjetas_idcliente_foreign` (`idcliente`),
  KEY `clientes_tarjetas_idtarjeta_foreign` (`idtarjeta`),
  KEY `clientes_tarjetas_idbanco_foreign` (`idbanco`),
  CONSTRAINT `clientes_tarjetas_idbanco_foreign` FOREIGN KEY (`idbanco`) REFERENCES `bancos` (`id`) ON DELETE CASCADE,
  CONSTRAINT `clientes_tarjetas_idcliente_foreign` FOREIGN KEY (`idcliente`) REFERENCES `clientes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `clientes_tarjetas_idtarjeta_foreign` FOREIGN KEY (`idtarjeta`) REFERENCES `tarjeta` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes_tarjetas`
--

LOCK TABLES `clientes_tarjetas` WRITE;
/*!40000 ALTER TABLE `clientes_tarjetas` DISABLE KEYS */;
INSERT INTO `clientes_tarjetas` VALUES (1,1,7,'Visa',5,'4567809987','Activo','2020-01-18 02:32:21','2020-01-18 02:32:21'),(2,2,8,'Pacificard',7,'678965456','Activo','2020-01-18 02:33:46','2020-01-18 02:33:46'),(3,3,8,'Pacificard',7,NULL,'Activo','2020-01-21 04:07:47','2020-01-21 04:07:47');
/*!40000 ALTER TABLE `clientes_tarjetas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compras`
--

DROP TABLE IF EXISTS `compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idproveedor` int(10) unsigned NOT NULL,
  `idusuario` int(10) unsigned NOT NULL,
  `tipo_identificacion` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_compra` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_compra` datetime NOT NULL,
  `impuesto` decimal(4,2) NOT NULL,
  `total` decimal(11,2) NOT NULL,
  `estado` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `compras_idproveedor_foreign` (`idproveedor`),
  KEY `compras_idusuario_foreign` (`idusuario`),
  CONSTRAINT `compras_idproveedor_foreign` FOREIGN KEY (`idproveedor`) REFERENCES `proveedores` (`id`),
  CONSTRAINT `compras_idusuario_foreign` FOREIGN KEY (`idusuario`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compras`
--

LOCK TABLES `compras` WRITE;
/*!40000 ALTER TABLE `compras` DISABLE KEYS */;
INSERT INTO `compras` VALUES (1,2,1,'Cedula','001','2019-09-30 00:00:00',0.12,30.00,'Registrado',NULL,NULL),(2,2,1,'FACTURA','002','2019-10-25 00:00:00',0.12,202.00,'Anulado','2019-10-26 07:19:07','2019-10-27 10:50:03'),(3,3,1,'FACTURA','003','2019-10-25 00:00:00',0.12,14.00,'Registrado','2019-10-26 07:20:37','2019-10-26 07:20:37'),(4,2,1,'FACTURA','004','2019-10-26 00:00:00',0.12,54.00,'Registrado','2019-10-27 10:51:37','2019-10-27 10:51:37'),(5,1,1,'FACTURA','005','2019-10-28 00:00:00',0.12,144.00,'Registrado','2019-10-29 04:02:35','2019-10-29 04:02:35'),(6,1,1,'FACTURA','6','2020-01-03 00:00:00',0.12,780.00,'Registrado','2020-01-03 05:35:46','2020-01-03 05:35:46'),(7,2,1,'FACTURA','10','2020-01-17 00:00:00',0.12,1560.00,'Registrado','2020-01-18 03:36:21','2020-01-18 03:36:21'),(8,3,1,'FACTURA','11','2020-01-19 00:00:00',0.12,780.00,'Registrado','2020-01-20 03:09:09','2020-01-20 03:09:09'),(9,1,1,'FACTURA','26','2020-03-21 00:00:00',0.12,670.00,'Anulado','2020-03-21 19:13:24','2020-11-20 08:27:23'),(10,3,5,'FACTURA','64278346826487','2020-11-19 00:00:00',0.12,12000.00,'Anulado','2020-11-19 16:25:10','2020-11-20 08:25:42'),(11,2,5,'FACTURA','3123213','2020-11-20 00:00:00',0.12,26200.00,'Registrado','2020-11-20 10:18:14','2020-11-20 10:18:14'),(12,3,5,'FACTURA','54525','2020-11-21 00:00:00',0.12,26200.00,'Registrado','2020-11-21 07:34:02','2020-11-21 07:34:02');
/*!40000 ALTER TABLE `compras` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ISASC`@`localhost`*/ /*!50003 TRIGGER `tr_updStockCompraAnular` AFTER UPDATE ON `compras` FOR EACH ROW BEGIN
  UPDATE productos p
    JOIN detalle_compras di
      ON di.idproducto = p.id
     AND di.idcompra = new.id
     set p.stock = p.stock - di.cantidad;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `detalle_compras`
--

DROP TABLE IF EXISTS `detalle_compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_compras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idcompra` int(10) unsigned NOT NULL,
  `idproducto` int(10) unsigned NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(11,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `detalle_compras_idcompra_foreign` (`idcompra`),
  KEY `detalle_compras_idproducto_foreign` (`idproducto`),
  CONSTRAINT `detalle_compras_idcompra_foreign` FOREIGN KEY (`idcompra`) REFERENCES `compras` (`id`) ON DELETE CASCADE,
  CONSTRAINT `detalle_compras_idproducto_foreign` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_compras`
--

LOCK TABLES `detalle_compras` WRITE;
/*!40000 ALTER TABLE `detalle_compras` DISABLE KEYS */;
INSERT INTO `detalle_compras` VALUES (1,1,1,5,12.34),(2,2,5,30,6.00),(3,2,3,20,1.10),(4,3,4,20,0.70),(5,4,3,30,1.10),(6,4,4,30,0.70),(7,5,4,80,0.70),(8,5,3,80,1.10),(9,6,5,100,6.00),(10,6,4,100,0.70),(11,6,3,100,1.10),(12,7,5,200,6.00),(13,7,4,200,0.70),(14,7,3,200,1.10),(15,8,5,100,6.00),(16,8,4,100,0.70),(17,8,3,100,1.10),(18,9,5,100,6.00),(19,9,4,100,0.70),(20,10,2,1,12000.00),(21,11,5,1,1200.00),(22,11,4,1,12000.00),(23,11,3,1,13000.00),(24,12,5,1,1200.00),(25,12,4,1,12000.00),(26,12,3,1,13000.00);
/*!40000 ALTER TABLE `detalle_compras` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ISASC`@`localhost`*/ /*!50003 TRIGGER `tr_updStockCompra` AFTER INSERT ON `detalle_compras` FOR EACH ROW BEGIN
 UPDATE productos SET stock = stock + NEW.cantidad 
 WHERE productos.id = NEW.idproducto;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `detalle_ventas`
--

DROP TABLE IF EXISTS `detalle_ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_ventas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idventa` int(10) unsigned NOT NULL,
  `idproducto` int(10) unsigned NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(11,2) NOT NULL,
  `descuento` decimal(11,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `detalle_ventas_idventa_foreign` (`idventa`),
  KEY `detalle_ventas_idproducto_foreign` (`idproducto`),
  CONSTRAINT `detalle_ventas_idproducto_foreign` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`id`),
  CONSTRAINT `detalle_ventas_idventa_foreign` FOREIGN KEY (`idventa`) REFERENCES `ventas` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_ventas`
--

LOCK TABLES `detalle_ventas` WRITE;
/*!40000 ALTER TABLE `detalle_ventas` DISABLE KEYS */;
INSERT INTO `detalle_ventas` VALUES (2,10,1,2,6.00,2.00),(3,11,1,1,1.25,0.00),(4,11,4,1,0.70,0.00),(5,12,3,3,1.10,2.00),(6,13,3,10,1.10,0.00),(7,14,5,6,6.00,0.00),(8,14,4,6,0.70,0.00),(9,15,5,5,6.00,0.00),(10,15,4,5,0.70,0.00),(11,16,4,5,0.70,0.00),(12,16,3,5,1.10,0.00),(13,17,5,5,6.00,0.00),(14,17,4,5,0.70,0.00),(15,18,5,5,6.00,0.00),(16,18,4,5,0.70,0.00),(17,19,5,5,6.00,0.00),(18,19,4,5,0.70,0.00),(19,20,5,5,6.00,0.00),(20,20,4,5,0.70,0.00),(21,21,5,5,6.00,0.00),(22,21,4,5,0.70,0.00),(23,22,5,5,6.00,0.00),(24,22,4,5,0.70,0.00),(25,23,5,5,6.00,0.00),(26,23,4,5,0.70,0.00),(27,24,5,2,6.00,0.00),(28,24,4,2,0.70,0.00),(29,25,5,5,6.00,0.00),(30,25,4,5,0.70,0.00),(31,26,5,8,6.00,0.00),(32,26,4,8,0.70,0.00),(33,27,5,5,6.00,0.00),(34,27,4,5,0.70,0.00),(35,28,4,5,0.70,0.00),(36,28,5,5,6.00,0.00),(37,29,5,5,6.00,0.00),(38,29,4,5,0.70,0.00),(39,30,5,10,6.00,0.00),(40,31,4,8,0.70,0.00),(41,32,4,5,0.70,0.00),(42,33,4,5,0.70,0.00),(43,34,4,5,0.70,0.00),(44,35,4,5,0.70,0.00),(45,36,5,5,6.00,0.00),(46,37,4,5,0.70,0.00),(47,38,5,5,6.00,0.00),(48,38,4,5,0.70,0.00),(49,39,4,1,12000.00,0.00),(50,40,3,1,13000.00,0.00),(51,40,2,1,12000.00,0.00),(52,40,1,1,19000.00,0.00),(53,41,3,1,13000.00,0.00),(54,41,2,1,12000.00,0.00),(55,42,3,1,13000.00,0.00),(56,42,1,1,19000.00,0.00),(57,43,1,1,19000.00,0.00),(58,43,3,1,13000.00,0.00);
/*!40000 ALTER TABLE `detalle_ventas` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ISASC`@`localhost`*/ /*!50003 TRIGGER `tr_updStockVenta` AFTER INSERT ON `detalle_ventas` FOR EACH ROW BEGIN
 UPDATE productos SET stock = stock - NEW.cantidad 
 WHERE productos.id = NEW.idproducto;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `forma_pago`
--

DROP TABLE IF EXISTS `forma_pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forma_pago` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `forma_pago_nombre_unique` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forma_pago`
--

LOCK TABLES `forma_pago` WRITE;
/*!40000 ALTER TABLE `forma_pago` DISABLE KEYS */;
INSERT INTO `forma_pago` VALUES (1,'Efectivo','Ventas en efectivo'),(2,'Cheque','Ventas en cheque'),(3,'Tarjeta','Ventas con tarjeta');
/*!40000 ALTER TABLE `forma_pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_100000_create_password_resets_table',1),(2,'2018_12_07_212527_create_categorias_table',1),(3,'2018_12_16_202808_create_productos_table',1),(4,'2018_12_19_144040_create_proveedores_table',1),(5,'2018_12_19_210020_create_clientes_table',1),(6,'2018_12_20_144948_create_roles_table',1),(7,'2018_12_20_000000_create_users_table',2),(12,'2018_12_27_214559_create_compras_table',3),(13,'2018_12_27_214622_create_detalle_compras_table',3),(14,'2019_10_31_022036_create_ventas_table',4),(15,'2019_10_31_022242_create_detalle_ventas_table',4),(19,'2019_12_25_064711_create_bancos_table',6),(28,'2019_12_31_184650_create_tarjeta_table',7),(34,'2019_12_25_222955_create_clientes_tarjetas_table',12),(35,'2019_12_26_141551_create_clientes_banco_table',12),(39,'2020_01_02_180001_create_pagos_table',13);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagos`
--

DROP TABLE IF EXISTS `pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `factura` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_pago` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idcliente` int(10) unsigned NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idbanco` int(10) unsigned NOT NULL,
  `nombre_banco` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idtarjeta` int(10) unsigned NOT NULL,
  `nombre_tarjeta` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valor` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pagos_idcliente_foreign` (`idcliente`),
  KEY `pagos_idbanco_foreign` (`idbanco`),
  KEY `pagos_idtarjeta_foreign` (`idtarjeta`),
  CONSTRAINT `pagos_idbanco_foreign` FOREIGN KEY (`idbanco`) REFERENCES `bancos` (`id`),
  CONSTRAINT `pagos_idcliente_foreign` FOREIGN KEY (`idcliente`) REFERENCES `clientes` (`id`),
  CONSTRAINT `pagos_idtarjeta_foreign` FOREIGN KEY (`idtarjeta`) REFERENCES `tarjeta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagos`
--

LOCK TABLES `pagos` WRITE;
/*!40000 ALTER TABLE `pagos` DISABLE KEYS */;
INSERT INTO `pagos` VALUES (1,'35','Cheque',3,'Cliente 2',7,'Pacifico',99,'Sin Tarjeta',30,'2020-03-23 16:07:41','2020-03-23 16:07:41'),(2,'35','Cheque',3,'Cliente 2',7,'Pacifico',99,'Sin Tarjeta',30,'2020-03-23 16:48:48','2020-03-23 16:48:48'),(3,'36','Cheque',3,'Cliente 2',7,'Pacifico',99,'Sin Tarjeta',4,'2020-03-23 17:37:05','2020-03-23 17:37:05'),(4,'37','Cheque',2,'Cliente 1',7,'Pacifico',99,'Sin Tarjeta',34,'2020-03-23 21:44:32','2020-03-23 21:44:32');
/*!40000 ALTER TABLE `pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idcategoria` int(10) unsigned NOT NULL,
  `codigo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio_venta` decimal(11,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT 1,
  `imagen` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `productos_nombre_unique` (`nombre`),
  KEY `productos_idcategoria_foreign` (`idcategoria`),
  CONSTRAINT `productos_idcategoria_foreign` FOREIGN KEY (`idcategoria`) REFERENCES `categorias` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,2,'346784','Motor de Reserva Mayta',19000.00,49,1,'ycBrAd460knD3cbv.jpg','2019-08-25 07:48:21','2020-11-17 22:23:06'),(2,2,'346784','Sistema Reserva E. Alanoca',12000.00,0,1,'WwafetAoJ0nZZcgI.jpg','2019-08-25 07:49:04','2020-11-17 22:21:23'),(3,2,'4578965','Motor de Compras',13000.00,1,1,'8ucukokkoxgqg4fl.png','2019-08-25 07:49:58','2020-11-11 13:36:44'),(4,2,'456745','Motor Hoteles',12000.00,-98,0,'gbxuZouMNhKn8W7L.jpg','2019-08-25 07:50:49','2020-11-19 16:21:43'),(5,1,'100','Sistema de contabilidad',1200.00,-97,1,'ugM5E7xXaEmamkrW.jpg','2019-08-25 07:51:43','2020-11-20 08:23:03');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_documento` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_documento` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `proveedores_nombre_unique` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` VALUES (1,'MegaLink','CEDULA','47893','La Paz Plaza Villa roel','28237865','mega@megalink.com','2019-08-25 07:54:31','2020-11-11 12:51:40'),(2,'SumaDevs','CEDULA','74329874923','Cdla Sauces 1 Mz F55 Villa 12','984394839','SumaDevs','2019-08-25 07:55:34','2020-11-11 12:50:20'),(3,'Linxs','CEDULA','0963452346','Ave. hilartar','2624356','linxs@linxs.com','2019-08-25 07:56:47','2020-11-11 12:49:41');
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_nombre_unique` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrador','Administrador',1,NULL,NULL),(2,'Vendedor','Vendedor',1,NULL,NULL),(3,'Comprador','Comprador',1,NULL,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tarjeta`
--

DROP TABLE IF EXISTS `tarjeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tarjeta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `externa` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idbancos` int(10) unsigned NOT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tarjeta_idbancos_foreign` (`idbancos`),
  CONSTRAINT `tarjeta_idbancos_foreign` FOREIGN KEY (`idbancos`) REFERENCES `bancos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tarjeta`
--

LOCK TABLES `tarjeta` WRITE;
/*!40000 ALTER TABLE `tarjeta` DISABLE KEYS */;
INSERT INTO `tarjeta` VALUES (7,'Visa','Visa','Nacional',5,1,NULL,NULL),(8,'Pacificard','Pacificar Pacifico','Nacional',7,1,NULL,NULL),(9,'Experta','Consumos Nacionales','NACIONAL',6,1,'2020-01-18 18:45:57','2020-01-18 18:45:57'),(99,'Sin Tarjeta','No existe tarjeta','Ninguna',99,1,NULL,NULL);
/*!40000 ALTER TABLE `tarjeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_documento` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_documento` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT 1,
  `idrol` int(10) unsigned NOT NULL,
  `imagen` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_usuario_unique` (`usuario`),
  UNIQUE KEY `users_password_unique` (`password`),
  KEY `users_idrol_foreign` (`idrol`),
  CONSTRAINT `users_idrol_foreign` FOREIGN KEY (`idrol`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Administrador','CEDULA','0914567854','Cdla. Sauces 2','0995643678','prueba123@hotmail.com','Admin1970','$2y$10$lo/iYvMVo9pSQhjyuBxrh.9JMAlUnnrVY1aXEEdNAZfWIyY5snK2.',1,1,'fW0LnERUSiPNeC5w.jpg','E9kgtJZyoSfVOm8qGECRLkyh4W49KfWy6bIM7vtqgMGJqPE2guBjqC6Jl2Pb','2019-08-25 08:01:59','2019-12-10 02:47:13'),(2,'Patricio Garzon','CEDULA','10347393848','Cdla Sauces 9 Mz F50 Villa 12','2624567','patriciogarzon40@gmail.com','patricio2','$2y$10$h.fKAyZeTrPoPwEH2wOLfuLEggO9Z.i.NGwpasDvqf77mD2Nkx3xW',1,2,'3P1vzdVk3alyMqoz.jpg',NULL,'2019-08-25 08:03:40','2019-12-09 06:18:28'),(3,'Lorena Gonzalez','CEDULA','104534875','Urb. Riberas del Batan','0451067456','lorena36@hotmail.com','jessileo1974','$2y$10$tZT58igSfw1VIWD6/apGSuTXkgEE.wdMLcv2fKlSZ/TWAIG0Uczwa',1,2,'nia1Yirf3biifYPE.jpg','Oem9t0EAIDcbWvWyt8eIJWKSO7eWT7E8MKrMeOj1bf0DQT93dO9XAnPPO9wO','2019-09-16 08:34:13','2019-12-09 06:17:54'),(4,'Jorge Zamora','CEDULA','09234576835','Cdla. Sauces 7','2237896','jorgezamora36@hotmail.com','jorge2019','$2y$10$OA/1EY7HhDOp.BFQXzNAGu0f8rg80KJ.ex3lEoa2OPp2tq0b8y8nC',1,2,'0iyfb0ztq1cC6WT4.jpg',NULL,'2019-12-08 14:37:46','2019-12-08 14:37:46'),(5,'Jimmy Mayta','CEDULA','623568989','Ninguna','Ninguna','jimmyymaytaj@gmail.com','jimmy123','$2y$10$iJcfDD8KqHks6uOPl5tmR.1tRifonryepYBE8bLJBQY32H2N0.zzW',1,1,'kxd8H5vuG6l97RNB.jpg','SllygfwoRIxaxGoq7ai7jrIBTkNCYBY3Me1zZCxfdHXbKapEFRYpIWRmCe5u','2020-11-04 01:25:32','2020-11-08 10:22:09'),(6,'yomar','CEDULA','473784973948','ave montesco','7839239082','yomar@gmail.com','yomar123','$2y$10$y6nh5i3ViMhbS.2bXPPuaOseoNlsgas2ombc403cPbSFPa64YlH0K',1,3,'qvKLfhMG6dQqTNPm.jpg','lsruphxdw0NXXUr5pg1tRzQIgIuudLNq9zbDpKCLtQjSop5KGZV3vy5WCaT4','2020-11-11 12:26:59','2020-11-12 02:27:16'),(7,'eva12345','CEDULA','46546465','vafadad','7465465','eva@gmail.com','eva12345','$2y$10$5RItwHzLQGFB.CAzcAidxe6jmeZ/m0Eq190ZYaHGOPcwSN0Mc8mCS',1,2,'UGJJmFg9OKsNr0wf.jpg','HWgEFBPBcnPfyl0IyACeD8A2UddHSLODklJndmGJbWY8GY2sKjadA6ee18HZ','2020-11-11 12:39:37','2020-11-21 09:55:05'),(8,'carmen','CEDULA','80198409328','jdklajdkls','234802398490','jksdjlkaf','gfgdsfgd','$2y$10$saVP6KeIw2B3sfzKfQCfKOVMFJGubGuP2hr4yjxG7QAn2kZvDN2a.',1,3,'2ypUJ60EfMMbcGFD.jpg',NULL,'2020-11-21 10:02:39','2020-11-21 10:03:39'),(9,'fdasd','CEDULA','adas','das','dasd','asda','dasd','$2y$10$f0KNmVtybX6tXffNWJe9D.F7JMhgBpHcQjI9rMcCyGwelchM6w6QW',1,1,'oLeJm28eu65zWDYf.jpg',NULL,'2020-11-21 12:01:21','2020-11-21 12:01:21');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventas`
--

DROP TABLE IF EXISTS `ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idcliente` int(10) unsigned NOT NULL,
  `idusuario` int(10) unsigned NOT NULL,
  `tipo_identificacion` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `num_venta` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_venta` datetime NOT NULL,
  `impuesto` decimal(4,2) NOT NULL,
  `total` decimal(11,2) NOT NULL,
  `estado` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ventas_idcliente_foreign` (`idcliente`),
  KEY `ventas_idusuario_foreign` (`idusuario`),
  CONSTRAINT `ventas_idcliente_foreign` FOREIGN KEY (`idcliente`) REFERENCES `clientes` (`id`),
  CONSTRAINT `ventas_idusuario_foreign` FOREIGN KEY (`idusuario`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventas`
--

LOCK TABLES `ventas` WRITE;
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
INSERT INTO `ventas` VALUES (10,1,2,'CEDULA','001','2019-11-11 00:00:00',2.34,6.00,'Registrado',NULL,NULL),(11,1,1,'FACTURA','002','2019-11-13 00:00:00',0.20,1.95,'Anulado','2019-11-14 01:46:12','2019-11-14 02:17:53'),(12,3,1,'FACTURA','003','2019-11-13 00:00:00',0.20,1.30,'Registrado','2019-11-14 02:19:10','2019-11-14 02:19:10'),(13,3,1,'FACTURA','10','2020-01-02 00:00:00',0.20,11.00,'Registrado','2020-01-03 04:11:31','2020-01-03 04:11:31'),(14,3,1,'FACTURA','18','2020-01-12 00:00:00',0.20,40.20,'Registrado','2020-01-12 07:51:14','2020-01-12 07:51:14'),(15,3,1,'FACTURA','20','2020-01-12 00:00:00',0.20,33.50,'Registrado','2020-01-12 19:01:50','2020-01-12 19:01:50'),(16,1,1,'FACTURA','39','2020-01-17 00:00:00',0.20,9.00,'Registrado','2020-01-18 03:13:24','2020-01-18 03:13:24'),(17,1,1,'FACTURA','15','2020-01-17 00:00:00',0.20,33.50,'Registrado','2020-01-18 03:38:02','2020-01-18 03:38:02'),(18,1,1,'FACTURA','15','2020-01-18 00:00:00',0.20,33.50,'Registrado','2020-01-18 17:57:58','2020-01-18 17:57:58'),(19,1,1,'FACTURA','16','2020-01-18 00:00:00',0.20,33.50,'Registrado','2020-01-18 19:00:23','2020-01-18 19:00:23'),(20,1,1,'FACTURA','17','2020-01-18 00:00:00',0.20,33.50,'Registrado','2020-01-19 02:29:27','2020-01-19 02:29:27'),(21,1,1,'FACTURA','17','2020-01-18 00:00:00',0.20,33.50,'Registrado','2020-01-19 03:07:17','2020-01-19 03:07:17'),(22,1,1,'FACTURA','17','2020-01-18 00:00:00',0.20,33.50,'Registrado','2020-01-19 03:08:46','2020-01-19 03:08:46'),(23,1,1,'FACTURA','17','2020-01-18 00:00:00',0.20,33.50,'Registrado','2020-01-19 03:44:26','2020-01-19 03:44:26'),(24,2,1,'FACTURA','19','2020-01-18 00:00:00',0.20,13.40,'Registrado','2020-01-19 04:00:30','2020-01-19 04:00:30'),(25,2,1,'FACTURA','20','2020-01-18 00:00:00',0.20,33.50,'Registrado','2020-01-19 04:23:12','2020-01-19 04:23:12'),(26,2,1,'FACTURA','21','2020-01-18 00:00:00',0.20,53.60,'Registrado','2020-01-19 04:50:26','2020-01-19 04:50:26'),(27,2,1,'FACTURA','20','2020-01-19 00:00:00',0.20,33.50,'Registrado','2020-01-19 13:32:52','2020-01-19 13:32:52'),(28,2,1,'FACTURA','23','2020-02-07 00:00:00',0.20,33.50,'Registrado','2020-02-07 23:24:07','2020-02-07 23:24:07'),(29,3,1,'FACTURA','22','2020-03-21 00:00:00',0.12,33.50,'Registrado','2020-03-21 19:16:54','2020-03-21 19:16:54'),(30,2,1,'FACTURA','26','2020-03-22 00:00:00',0.12,60.00,'Registrado','2020-03-23 01:30:36','2020-03-23 01:30:36'),(31,3,1,'FACTURA','30','2020-03-22 00:00:00',0.12,5.60,'Registrado','2020-03-23 03:03:59','2020-03-23 03:03:59'),(32,2,1,'FACTURA','31','2020-03-22 00:00:00',0.12,3.50,'Registrado','2020-03-23 03:34:17','2020-03-23 03:34:17'),(33,2,1,'FACTURA','32','2020-03-22 00:00:00',0.12,3.50,'Registrado','2020-03-23 03:49:52','2020-03-23 03:49:52'),(34,2,1,'FACTURA','33','2020-03-22 00:00:00',0.12,3.50,'Registrado','2020-03-23 04:44:28','2020-03-23 04:44:28'),(35,2,1,'FACTURA','34','2020-03-23 00:00:00',0.12,3.50,'Registrado','2020-03-23 05:21:31','2020-03-23 05:21:31'),(36,3,1,'FACTURA','35','2020-03-23 00:00:00',0.12,30.00,'Registrado','2020-03-23 16:48:51','2020-03-23 16:48:51'),(37,3,1,'FACTURA','36','2020-03-23 00:00:00',0.12,3.50,'Registrado','2020-03-23 17:37:09','2020-03-23 17:37:09'),(38,2,1,'FACTURA','37','2020-03-23 00:00:00',0.12,33.50,'Registrado','2020-03-23 21:44:41','2020-03-23 21:44:41'),(39,4,5,'FACTURA','32331212','2020-11-11 00:00:00',0.12,12000.00,'Registrado','2020-11-12 03:03:14','2020-11-12 03:03:14'),(40,4,5,'FACTURA','413132323','2020-11-21 00:00:00',0.12,44000.00,'Anulado','2020-11-21 07:35:47','2020-11-21 08:19:29'),(41,2,5,'FACTURA','31321','2020-11-21 00:00:00',0.12,25000.00,'Registrado','2020-11-21 08:42:46','2020-11-21 08:42:46'),(42,1,5,'FACTURA','3123','2020-11-21 00:00:00',0.12,32000.00,'Registrado','2020-11-21 09:43:04','2020-11-21 09:43:04'),(43,1,5,'TICKET','12341234','2020-11-21 00:00:00',0.12,32000.00,'Anulado','2020-11-21 09:47:17','2020-11-21 10:46:50');
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ISASC`@`localhost`*/ /*!50003 TRIGGER `tr_updStockVentaAnular` AFTER UPDATE ON `ventas` FOR EACH ROW BEGIN
  UPDATE productos p
    JOIN detalle_ventas dv
      ON dv.idproducto = p.id
     AND dv.idventa= new.id
     set p.stock = p.stock + dv.cantidad;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-23 11:58:51
