
# Sistema de Información para la Administración y Control de Ventas
## Information System for Administration and Sales Control (ISASC)

**Sistema de Información para la Administración y Control de Ventas**

Las tecnologías de la información están transformando las actividades económicas y
cotidianas. Indiscutiblemente los sistemas de información han invadido a todos y cada uno
de los campos de la actividad humana: ciencia tecnología, arte, educación, recreación,
administración y economía la tecnología relacionada con el manejo de la información hace
que más personas y servicios estén conectados e interactúen entre si las tareas y funciones
que tradicionalmente se realizaban manualmente ahora son hechas por medio de un sistema
de información.

Linxs SRL es una empresa dedicada al desarrollo de aplicaciones de escritorio, páginas web,
sistemas de reservas, material de publicidad, ventas de espacios publicitarios y motores, se
encuentra en el mercado desde el año 2005 y ha ganado experiencia atendiendo a decenas de
clientes en Bolivia, Estados Unidos y Puerto Rico. Ha diseñado páginas web, CD Multimedia
y material de Merchandising para los más variados rubros y empresas, desde Hotelería y
Turismo, hasta Industria, Banca y Gastronomía. Conocemos de calidad. Actualmente cuenta
con alrededor de 7500 clientes en los nueve departamentos de Bolivia.

El presente proyecto tiene como objetivo desarrollar un sistema de información para la
administración y control de ventas de manera que pueda ayudar en la administración y el
control de ventas de la empresa Linxs utilizando metodología OOHDM que va enfocado al
desarrollo exclusivo de sistemas de información.
Asimismo, se emplearon los factores de calidad (Métodos de evaluación de calidad). Para
evaluar y medir la calidad del producto se aplicó la norma ISO 25000 y sus criterios de
evaluación correspondientes.

Finalmente, los objetivos planteados en base a la problemática existente, han sido alcanzados
de manera satisfactoria, de manera que se produjo un sistema de calidad que permite
administrar y controlas las ventas a través del sistema.

## **Lenguajes**
- PHP
- JavaScript
- Sass

## **Framework**
- Laravel
- Vue
- mdb
- Bootstrap
- FPDF

**Ejecutar**

Terminal 1
```
php artisan serve
```
Terminal 2
```
npm run watch
```
Limpiar
```
php artisan cache:clear
php artisan config:clear
php artisan config:cache
```
Recompilar
```
npm rebuild
```

<!-- <div align="center">
  <img src="resources/images/2008301938.jpg">
</div> -->

![Philadelphia's Magic Gardens. This place was so cool!](resources/images/2008301938.jpg "Jimmy Mayta")

































