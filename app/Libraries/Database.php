<?php
namespace App\Libraries;

class Database {
    public static function Table($Table) {
        return "\"$Table\"";
    }

    public static function Column($Column) {
        return "\"$Column\"";
    }

    public static function Procedure($Procedure) {
        return "\"$Procedure\"";
    }
}
