<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\FPDF\FPDF;
use Illuminate\Support\Facades\DB;
use App\Libraries\App;
use Illuminate\Support\Facades\Auth;

class Reports extends Controller {
    public function ReportClients() {
        $Clientes = DB::table('Clientes')->get();

        $pdf=new PDF_MC_Table('P', 'mm', 'Letter');
        $pdf->AddPage();
        $pdf->AliasNbPages();
        $pdf->SetMargins(12, 12, 12);

        $pdf->Rect(12, 12, 194, 4, 'D');

        $pdf->SetFont('Arial', false, 12);
        $pdf->SetXY(12, 8);
        $pdf->Cell(153, 4, utf8_decode(Auth::user()->nombre), 1, 0,'L'); // modifcar 100
        $pdf->Cell(41, 4, date('d/m/Y H:i:s', strtotime(App::DateTime())), 1, 0, 'R'); //modifiar 60

        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetXY(12, 12);
        $pdf->MultiCell(194, 4, 'Lista de Clientes', 0, 'C', false); // media original

        $Anc = [43, 38, 40, 25, 48, 30]; // modificar
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell($Anc[0], 4, utf8_decode('Nombres'), 1, 0, 'C');
        $pdf->Cell($Anc[1], 4, utf8_decode('Cedula Identidad'), 1, 0, 'C');
        $pdf->Cell($Anc[2], 4, utf8_decode('Dirección'), 1, 0, 'C');
        $pdf->Cell($Anc[3], 4, utf8_decode('Tel/Cel'), 1, 0, 'C');
        $pdf->Cell($Anc[4], 4, utf8_decode('Email'), 1, 1, 'C');

        $pdf->SetFont('Arial', '', 12);

        $pdf->SetWidths($Anc);

        foreach ($Clientes as $Cli) {
            $pdf->Row([
                utf8_decode($Cli->nombre),
                utf8_decode($Cli->num_documento),
                utf8_decode($Cli->direccion),
                utf8_decode($Cli->telefono),
                utf8_decode($Cli->email)
            ]);
        }
        // ob_end_clean();
        $pdf->Output('I', 'Clientes.pdf', true);
        ob_end_flush();
    }

    public function ReportProducts() {
        $Clientes = DB::table('Productos')->get();

        $pdf=new PDF_MC_Table('P', 'mm', 'Letter');
        $pdf->AddPage();
        $pdf->AliasNbPages();
        $pdf->SetMargins(12, 12, 12);

        $pdf->Rect(12, 12, 194, 4, 'D');

        $pdf->SetFont('Arial', false, 12);
        $pdf->SetXY(12, 8);
        $pdf->Cell(153, 4, Auth::user()->nombre, 1, 0,'L'); // modifcar 100
        $pdf->Cell(41, 4, date('d/m/Y H:i:s', strtotime(App::DateTime())), 1, 0, 'R'); //modifiar 60

        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetXY(12, 12);
        $pdf->MultiCell(194, 4, 'Lista de Productos', 0, 'C', false); // media original

        $Anc = [64, 66, 64]; // modificar
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell($Anc[0], 4, utf8_decode('Codigo'), 1, 0, 'C');
        $pdf->Cell($Anc[1], 4, utf8_decode('Producto'), 1, 0, 'C');
        $pdf->Cell($Anc[2], 4, utf8_decode('Precios (Bs)'), 1, 1, 'C');

        $pdf->SetFont('Arial', '', 12);

        $pdf->SetWidths($Anc);

        foreach ($Clientes as $Cli) {
            $pdf->Row([
                utf8_decode($Cli->codigo),
                utf8_decode($Cli->nombre),
                utf8_decode($Cli->precio_venta)
            ]);
        }
        // ob_end_clean();
        $pdf->Output('I', 'Clientes.pdf', true);
        ob_end_flush();
    }
}

class PDF_MC_Table extends FPDF {
    var $widths;
    var $aligns;

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }

    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
    }

    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }
}
