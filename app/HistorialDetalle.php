<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialDetalle extends Model
{
    protected $table = 'historial_detalle';
    protected $fillable = [
        'id',
        'idusuario',
        'id_data',
        'historial',
        'historial_detalle',
        'descripcion',
        'acccion',
        'fecha'
    ];
    public $autoincrement = false;
    public $timestamps = false;
}
